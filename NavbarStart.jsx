import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavbarStart extends Component {
  state = {
    pics:
      "https://st4.depositphotos.com/15317184/23269/v/1600/depositphotos_232696824-stock-illustration-book-icon-vector-solid-illustration.jpg",
  };
  render() {
    return (
      <nav
        style={{ width: "auto", top: 0, right: 0, left: 0 }}
        className="navbar navbar-expand-lg navbar-light bg-light"
      >
        <img
          src={this.state.pics}
          width="70"
          height="70"
          className="d-inline-block align-top"
          alt="React Bootstrap logo"
        />

        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle active"
                href="#"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Options
              </a>
              <ul
                className="dropdown-menu "
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link
                    className=" navbar-brand"
                    to="/books?searchText=Harry%20Potter%20Books&startIndex=0&maxResults=8"
                  >
                    Harry Potter Books
                  </Link>
                </li>
                <li>
                  <Link
                    className=" navbar-brand"
                    to="/books?searchText=Books%20by%20Agatha%20Christie&startIndex=0&maxResults=8"
                  >
                    Books by Agatha Christie
                  </Link>
                </li>
                <li>
                  <Link
                    className=" navbar-brand"
                    to="/books?searchText=Books%20by%20Premchand&startIndex=0&maxResults=8"
                  >
                    Books by Premchand
                  </Link>
                </li>
                <li>
                  <Link
                    className=" navbar-brand"
                    to="/books?searchText=Love%20Stories%20by%20Jane&startIndex=0&maxResults=8"
                  >
                    Love Stories by Jane
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavbarStart;
