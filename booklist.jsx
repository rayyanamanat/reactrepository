import React, { Component, StrictMode } from "react";
import NavbarStart from "./NavbarStart";
//import Booklist from "./booklist";
import queryString from "query-string";
import LeftPanelForm from "./LeftPanelForm";
import http from "./httpService";
import configs from "./configs.json";

class List extends Component {
  state = {
    view: 1,
    posts: [],
    language: [
      { name: "English", value: "en" },
      { name: "French", value: "fr" },
      { name: "Hindi", value: "h" },
    ],

    filterBtn: [
      { name: "Full Volume", value: "full" },
      { name: "Free Google e-books", value: "free-ebooks" },
      { name: "Paid Google e-books", value: "paid-ebooks" },
    ],

    check: "",
    totalItems: "",
  };

  async componentDidMount() {
    let str = configs.apiEndpoint;
    let { searchText, filter, page } = queryString.parse(
      this.props.location.search
    );
    searchText = searchText ? searchText.split(" ").join("%20") : "";
    str = str + "?q=" + searchText;
    const { data: posts } = await http.get(str + this.props.location.search);
    console.log("String", str + this.props.location.search);

    this.setState({
      posts: posts.items,
      check: this.props.location.key,
      totalItems: posts.totalItems,
    });
  }

  async componentDidUpdate() {
    if (this.state.check != this.props.location.key) {
      let str = configs.apiEndpoint;
      let { searchText, filter, page } = queryString.parse(
        this.props.location.search
      );
      str = str + "?q=" + searchText;

      const { data: posts } = await http.get(str + this.props.location.search);
      console.log("String", str + this.props.location.search);
      this.setState({
        posts: posts.items,
        check: this.props.location.key,
        totalItems: posts.totalItems,
      });
    }
  }

  handleOptionChange = (filterRadio, langRadio) => {
    let filterName = filterRadio.selectedFilter;
    let languageName = langRadio.selectedLanguage;
    let { searchText, startIndex, maxResults } = queryString.parse(
      this.props.location.search
    );
    console.log("searchtextt", searchText);
    this.callURL(
      "",
      filterName,
      languageName,
      searchText,
      startIndex,
      maxResults
    );
  };

  handleUpdate(upd) {
    let {
      searchText,
      startIndex,
      maxResults,
      langRestrict,
      filter,
    } = queryString.parse(this.props.location.search);
    langRestrict = langRestrict ? langRestrict : "";
    filter = filter ? filter : "";

    startIndex = parseInt(startIndex);
    maxResults = parseInt(maxResults);
    if (upd == 1) {
      if (maxResults + 8 <= this.state.totalItems) {
        startIndex = startIndex + 8;
        maxResults = maxResults + 8;
      }
    }
    if (upd == -1) {
      if (startIndex - 8 >= 0) maxResults = maxResults - 8;
      startIndex = startIndex - 8;
    }
    this.callURL("", filter, langRestrict, searchText, startIndex, maxResults);
  }

  callURL = (
    params,
    filterName,
    languageName,
    searchText,
    startIndex,
    maxResults
  ) => {
    let path = "/books";

    startIndex = startIndex ? startIndex : "0";
    maxResults = maxResults ? maxResults : "8";

    params = this.addToParams(params, "searchText", searchText);
    params = this.addToParams(params, "startIndex", startIndex);
    params = this.addToParams(params, "maxResults", maxResults);

    params = this.addToParams(params, "filter", filterName);
    params = this.addToParams(params, "langRestrict", languageName);

    console.log("path,params", path, params);
    this.props.history.push({ search: params });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : "&" + params;

      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  makeLanguageRadioStructure(language, langRestrict) {
    let sportsRadio = {
      Languages: language,
      selectedLanguage: langRestrict,
    };

    return sportsRadio;
  }
  makeFilterRadioStructure(filtersBtn, filter) {
    let sportsRadio = {
      filters: filtersBtn,
      selectedFilter: filter,
    };

    return sportsRadio;
  }

  handleCategories(arr) {
    console.log("arr", arr);
    arr = arr ? arr : "NA";
    
    return arr;
  }

  render() {
    let { language, filterBtn } = this.state;

    let { startIndex, maxResults, langRestrict, filter } = queryString.parse(
      this.props.location.search
    );
    langRestrict = langRestrict ? langRestrict : "";
    filter = filter ? filter : "";

    startIndex = parseInt(startIndex) + 1;

    let langRadio = this.makeLanguageRadioStructure(language, langRestrict);
    let filterRadio = this.makeFilterRadioStructure(filterBtn, filter);
    
    console.log("posts", this.state.posts);
    return (
      <div className="container">
        <div className="row border">
          <div className="col-3">
            <LeftPanelForm
              filterRadio={filterRadio}
              langRadio={langRadio}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <div className="App " style={{ color: "yellow", fontSize: "30px" }}>
              All Books
            </div>
            <div style={{ color: "green", fontSize: "20px" }}>
              {startIndex}-{maxResults}
            </div>
            <div className="row">
              {this.state.posts.map((s) => (
                <div
                  key={s.id}
                  className="col-3 m-1"
                  style={{
                    background: "green",
                    width: 200,
                    height: 300,
                  }}
                >
                  <img
                    src={s.volumeInfo.imageLinks.smallThumbnail}
                    style={{
                      marginLeft: 20,
                      width: 100,
                      height: 100,
                    }}
                  />
                  <div
                    style={{
                      marginLeft: 20,
                      fontWeight: "bold",
                    }}
                  >
                    {s.volumeInfo.title}
                  </div>
                  <br />
                  <div
                    style={{
                      marginLeft: 20,
                    }}
                  >
                    {s.volumeInfo.authors}
                  </div>

                  <br />
                  <div
                    style={{
                      marginLeft: 20,
                    }}
                  >
                    {this.handleCategories(s.volumeInfo.categories)}
                  </div>
                </div>
              ))}
            </div>
            <div className="row">
              {startIndex - 1 > 0 ? (
                <button
                  className="btn btn-warning"
                  onClick={() => this.handleUpdate(-1)}
                  style={{ marginRight: 200 }}
                >
                  Prev
                </button>
              ) : (
                ""
              )}
              {maxResults < this.state.totalItems ? (
                <button
                  className="btn btn-warning"
                  onClick={() => this.handleUpdate(1)}
                  style={{ marginLeft: 200 }}
                >
                  Next
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default List;
