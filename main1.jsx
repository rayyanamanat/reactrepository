import React, { Component } from "react";
import NavbarStart from "./NavbarStart";
import List from "./booklist";
//import http from "./services/httpService";
//import configs from "./configs.json";
//import Playerdetails from "./Playerdetails";
import Main from "./main";
//import MainBok1 from "./mainBok1";
import { Route, Switch, Redirect } from "react-router-dom";

import "./App.css";

//import MainStar from "./mainstar";

class Main1 extends Component {
  /*  state = {
    formdata: { name: "" },
    lib:
      "https://st4.depositphotos.com/15317184/23269/v/1600/depositphotos_232696824-stock-illustration-book-icon-vector-solid-illustration.jpg",
    view: 1,
  };
*/
  render() {
    return (
      <div className="container">
        <div>
          <NavbarStart />
          <Switch>
            <Route path="/books" component={List} />
            <Route to="" component={Main} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main1;
