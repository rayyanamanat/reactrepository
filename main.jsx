import React, { Component } from "react";
import NavbarStart from "./NavbarStart";
//import Booklist from "./booklist";
//import http from "./services/httpService";
//import configs from "./configs.json";
//import Playerdetails from "./Playerdetails";
//import MainBok1 from "./mainBok1";
//import { Route, Switch, Redirect } from "react-router-dom";

//import "./App.css";

//import MainStar from "./mainstar";

class Main extends Component {
  state = {
    formdata: { name: "" },
    lib:
      "https://st4.depositphotos.com/15317184/23269/v/1600/depositphotos_232696824-stock-illustration-book-icon-vector-solid-illustration.jpg",
    view: 1,
  };

  handleChange = (e) => {
    //    const { currentTarget: input } = e;
    //    console.log("input", input);
    let data = { ...this.state.formdata };

    data[e.currentTarget.name] = e.currentTarget.value;
    console.log("type", e.currentTarget);
    console.log("data", data);
    this.setState({ formdata: data });
  };
  handleSearch() {
    //    console.log("Search", this.state.formdata.name);
    let path = "/books";
    //    this.setState({ view: 2 });
    this.props.history.push({
      pathname: path,
      search:
        "searchText=" + this.state.formdata.name + "&startIndex=0&maxResults=8",
    });
  }

  render() {
    return (
      <div className="container">
        <div className="App">
          <img
            src={this.state.lib}
            style={{
              marginTop: 20,
              width: 100,
              height: 100,
              backgroundColor: "#00BCD4",
              borderRadius: 50,
              transform: [{ scaleX: 2 }],
            }}
          />
        </div>
        <form className="">
          <div className="form-group">
            <input
              value={this.state.formdata.name}
              type="text"
              name="name"
              id="name"
              onChange={this.handleChange}
              className="form-control mt-10"
            />
          </div>
          <button
            onClick={() => this.handleSearch()}
            className="btn btn-primary"
          >
            Search
          </button>
        </form>
      </div>
    );
  }
}

export default Main;
