import React, { Component } from "react";

class LeftPanelForm extends Component {
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { filterRadio, langRadio } = this.props;

    if (input.name === "selectedLanguage") {
      langRadio[input.name] = input.value;
    }
    if (input.name === "selectedFilter") {
      filterRadio[input.name] = input.value;
    }

    //console.log("filterRadio, langRadio", filterRadio, langRadio);

    this.props.onOptionChange(filterRadio, langRadio);
  };

  render() {
    const { filterRadio, langRadio } = this.props;
    //console.log("filterRadio, langRadio", filterRadio, langRadio);
    return (
      <div>
        <form>
          <div>
            <div
              className="bg-light border"
              style={{ fontWeight: "bold", marginTop: 20 }}
            >
              Language
            </div>
            {langRadio.Languages.map((item) => (
              <div
                key={item.value}
                style={{ height: "40px" }}
                className="form-check border"
              >
                <input
                  value={item.value}
                  style={{ marginTop: 15 }}
                  onChange={this.handleChange}
                  id={item.name}
                  type="radio"
                  name="selectedLanguage"
                  checked={item.value === langRadio.selectedLanguage}
                  className="form-check-input"
                />
                <label
                  style={{ marginTop: 10 }}
                  className="form-check-label"
                  htmlFor={item.name}
                >
                  {item.name}
                </label>
              </div>
            ))}
          </div>
          <br />
          <div
            className="bg-light border"
            style={{ fontWeight: "bold", marginTop: 20 }}
          >
            Filter
          </div>
          {filterRadio.filters.map((item) => (
            <div key={item.value} className="form-check border">
              <input
                style={{ marginTop: 15 }}
                value={item.value}
                onChange={this.handleChange}
                id={item.name}
                type="radio"
                name="selectedFilter"
                checked={item.value === filterRadio.selectedFilter}
                className="form-check-input"
              />
              <label
                style={{ marginTop: 10 }}
                className="form-check-label"
                htmlFor={item.name}
              >
                {item.name}
              </label>
            </div>
          ))}
        </form>
      </div>
    );
  }
}

export default LeftPanelForm;
